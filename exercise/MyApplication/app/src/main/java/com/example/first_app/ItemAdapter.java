package com.example.first_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
//import android.widget.ListView;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter {

      LayoutInflater mInflater;
      String[] items;
      String[] prices;
      String[] descriptions;

      public ItemAdapter(Context c, String[] i, String[] p, String[] d) {
          items = i;
          prices = p;
          descriptions = d;
          mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      }

      @Override
      public int getCount() {
          return items.length;
      }

      @Override
      public Object getItem(int i) {
          return items[i];
      }

      @Override
      public long getItemId(int i) {
          return i;
      }

      @Override
      public View getView(int i, View view, ViewGroup viewGroup) {
          View v = mInflater.inflate(R.layout.list_view_details, null);
          TextView nameTextView = (TextView) v.findViewById(R.id.nameTextViews);
          TextView descriptionTextView = (TextView) v.findViewById(R.id.descriptionTextViews);
          TextView priceTextView = (TextView) v.findViewById(R.id.priceTextViews);

           String name = items[i];
           System.out.println(name);
           String desc = descriptions[i];
           System.out.println(desc);
           String cost = prices[i];
           System.out.println(cost);

           nameTextView.setText(name);
           descriptionTextView.setText(desc);
           priceTextView.setText(cost);

           return v;
      }
  }
