package com.example.first_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListViewActivity extends AppCompatActivity {

    ListView myListView;
    String[] items;
    String[] prices;
    String[] descriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        Resources res = getResources();
        myListView = (ListView) findViewById(R.id.myListViews);
        items = res.getStringArray(R.array.items);
        prices = res.getStringArray(R.array.prices);
        descriptions = res.getStringArray(R.array.descriptions);

       ItemAdapter itemAdapter = new ItemAdapter(this, items, prices, descriptions);
       myListView.setAdapter(itemAdapter);

       myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
               //Intent startIntent = new Intent(getApplicationContext(), SecondActivity.class);
               Intent showDetailActivity = new Intent(getApplicationContext(), DetailActivity.class);
               showDetailActivity.putExtra("ITEM_INDEX", i);
               startActivity(showDetailActivity);
           }
       });
    }
}