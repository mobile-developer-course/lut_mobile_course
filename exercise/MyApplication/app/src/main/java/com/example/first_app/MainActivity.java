package com.example.first_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstNumEditText = (EditText) findViewById(R.id.firstNumEditText);
                EditText secondNumEditText = (EditText) findViewById(R.id.secondNumEditText);
                TextView resultTextField = (TextView) findViewById(R.id.resultTextField);

                int num1 = Integer.parseInt(firstNumEditText.getText().toString());
                int num2 = Integer.parseInt(secondNumEditText.getText().toString());
                int result = num1 + num2;
                resultTextField.setText(result + "");


            }
        });

        // Attempt to launch an activity within our own app
        Button secondActivityBtn = (Button)findViewById(R.id.secondActivityBtn);
        secondActivityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), SecondActivity.class);

                // show how to pass information to second activity
                startIntent.putExtra("Something", "Hello World");
                startActivity(startIntent);
            }

        });

        // Attempt to launch an activity within our own app
        Button listViewBtn = (Button)findViewById(R.id.listViewBtn);
        listViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startListIntent = new Intent(getApplicationContext(), ListViewActivity.class);

                // show how to pass information to second activity
               // startIntent.putExtra("Something", "Hello World");
                startActivity(startListIntent);
            }

        });

        // Attempt to launch an activity outside our app
        Button googleBtn = (Button)findViewById(R.id.googleBtn);
        googleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.google.com";
                //Uri webaddress = Uri.parse(url);
                Intent gotoGoogle = new Intent(Intent.ACTION_VIEW);
                if (gotoGoogle.resolveActivity(getPackageManager()) != null) {
                    gotoGoogle.setData(Uri.parse(url));
                    startActivity(gotoGoogle);
                }
            }
      });

    }
}