package com.example.first_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second2);

        if (getIntent().hasExtra("Something"));

        TextView tv = (TextView) findViewById(R.id.helloWorld);
        String text = getIntent().getExtras().getString("Something");
        tv.setText(text);
    }
}