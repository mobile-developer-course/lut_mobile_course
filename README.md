# README #

This readme document is explaining exercise work done for this course.

### What is this repository for? ###

* This repository contains the Android mobile developer course exercises work by Zahir Babur.

### How do I get set up? ###

* Install Android studio.
* Install Java
* Install SDK

### Excerises work ###

* In exercises following functionality has been implemented.
  * New Project
  * Multiple activities pages
  * Navigation between activities
  * Text view and button implementation
  * Listview and image view implementation
  * Navigation to browser window

### Screenshots ###
   [![Screenshot-20210711-074828.png](https://i.postimg.cc/HkrsqwnJ/Screenshot-20210711-074828.png)](https://postimg.cc/TKX6mWrG)
   [![Screenshot-20210711-075455.png](https://i.postimg.cc/FRNvH3rV/Screenshot-20210711-075455.png)](https://postimg.cc/QB4w6BWF)
   [![Screenshot-20210711-080027.png](https://i.postimg.cc/VsZx33CR/Screenshot-20210711-080027.png)](https://postimg.cc/t7Pv6vDZ)